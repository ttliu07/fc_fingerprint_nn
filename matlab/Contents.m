% Contents of matlab file for
%
% Functional Connectome Fingerprinting Using Shallow Feedforward Neural Networks
% G. Sarar, B. Rao, T.T. Liu
%
% https://www.biorxiv.org/content/10.1101/2020.10.19.346189v1
%
% MAIN FUNCTIONS;
%
% make_fig1_fingerprint : make up Figure 1 plots
% make_fig2_fingerprint : make up Figure 2 plots
%
% fcnn_roi_regions: look at top 60 ROIs and their assignment to regions. 
%
% UTILITY Functions:
%
% modnum:  calculates number of model parameters
% plotboxpos: helper function for Figure 2 plots

