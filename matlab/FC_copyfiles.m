% copy over files for public repo


%%
roivals = [ 30   50 60  100 120 200 200 300 300 379 379 ];
spanvals = [200 200 100 100 50   50 30  20   34  27  16 ];
unitnum =  [326 256  1024 256 1024 512 6568 4096 8192 8192 14860];

nVals = length(roivals);
suf1vec = {'corrNN','normNN'};
%suf1vec = {'corrNN'};
nsuf1 = length(suf1vec);
for iSuf1 = 1:nsuf1
    suf1 = suf1vec{iSuf1};offset =(iSuf1-1)*nVals;
    for iVal = 1:nVals
        roiNum = roivals(iVal);
        spanNum = spanvals(iVal);
        uNum = unitnum(iVal);
      
        if iSuf1 == 1
            fnames{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl_gsr1.pckl',spanNum,roiNum,suf1);
            fnamesGS{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl_gsr0.pckl',spanNum,roiNum,suf1);
        else
            fnames{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl1_u%d_sl0_g1_r0_gsr1.pckl',spanNum,roiNum,suf1,uNum);
            fnamesGS{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl1_u%d_sl0_g1_r0_gsr0.pckl',spanNum,roiNum,suf1,uNum);
        end
        
    end;
end

srcdir = '/Users/ttl/gitrepos/simplified_model_fingerprinting_repo/matlab/StatsData2/'
sdir = 'StatsData2/';  % local directory where I've put a copy of the data from AWS
nFiles = length(fnames);

for iFile = 1:nFiles
    system(sprintf('cp %s%s %s%s',srcdir,fnames{iFile},sdir,fnames{iFile}))
    system(sprintf('cp %s%s %s%s',srcdir,fnamesGS{iFile},sdir,fnamesGS{iFile}))
end