% Script to look at top ROIs and assign to Glasser brain regions
%
% Functional Connectome Fingerprinting Using Shallow Feedforward Neural Networks
% G. Sarar, B. Rao, T.T. Liu
clear;
ddir = 'ROIdata/';
fnames = {'indexesPerm_correlation_greedy_ROI_l2_findingIndexesResults.pckl',...
    'greedy_ROI_l2_findingIndexesResults.pckl'};

nFiles = length(fnames);

for iFile = 1:nFiles
    fid = py.open([ddir,fnames{iFile}],'rb');
    greedy{iFile} = py.pickle.load(fid); 
    st{iFile} = struct(greedy{iFile});
    scores{iFile} = cell2mat(cell(st{iFile}.controlled_scores));
    gr_index{iFile} = cell2mat(cell(st{iFile}.killedIndexes));
end

load ROIdata/regions.mat   % load variable regions  -- 360 region names   1x360 cell array
load ROIdata/parcellation_info.mat  % load variable raw   180x3 cell array
load ROIdata/primary_sections.mat   % load variable primary_sections


clear gind subcort_ind sortNames fullNames

regNames = {'1. Primary Visual Cortex','2. Early Visual Cortex','3. Dorsal Stream Visual Cortex','4. Ventral Stream Visual Cortex',...
    '5. MT+ Complex and Neighboring Visual Areas','6. Somatosensory and Motor Cortex','7. Paracentral Lobular and Mid Cingulate Cortex',...
    '8. Premotor Cortex','9. Posterior Opercular Cortex','10. Early Auditory Cortex',...
    '11. Auditory Association Cortex','12. Insular and Frontal Opercular Cortex', '13. Medial Temporal Cortex',...
    '14. Lateral Temporal Cortex','15. Temporo-Parieto-Occipital Junction','16. Superior Parietal Cortex',...
    '17. Inferior Parietal Cortex','18 Posterior Cingulate Cortex','19. Anterior Cingulate and Medial Prefrontal Cortex',...
    '20. Orbital and Polar Frontal Cortex','21. Inferior Frontal Cortex','22. Dorsolateral Prefrontal Cortex'};
    
    
    
Nregions = length(gr_index{1});
PrimReg = NaN*ones(Nregions,2);
for k = 1:2
    gind{k} = gr_index{k}';
    gind{k}= flipud(gind{k})+1;  % add 1 since Python indexes from 0 to 378
    % find subcortical region -- this shows for corr first sbucort is at
    % rank 150 and for norm it is at rank 99
    subcort_ind{k} = find(gind{k} > 360);
    subcort_ind{k}';
    
    % make the list of sorted regions
    for iCort = 1:Nregions
        thisInd = gind{k}(iCort);
        if thisInd <= 360
            sortNames{k,iCort} = regions{thisInd};
            regInd = rem(thisInd,180);if regInd == 0;regInd = 180;end;
            fullNames{k,iCort} = raw{regInd,2};
            PrimReg(iCort,k) = primary_sections(regInd);
        else
            sortNames{k,iCort} = 'subCort';
        end
    end
end

% print ouf the first top 60 regions for both corrNN and normNN

fprintf('\nCorrNN regions\n-----------------\n');

for iCort = 1:60
   fprintf('%d: Ind[%d]%s(%s) Reg: %d %s  \n',iCort,gind{1}(iCort),sortNames{1,iCort},fullNames{1,iCort},PrimReg(iCort,1),regNames{PrimReg(iCort,1)});
end

fprintf('\nNormNN regions\n-----------------\n');

for iCort = 1:60
   fprintf('%d Ind[%d]%s(%s) Reg: %d %s   \n',iCort,gind{2}(iCort), sortNames{2,iCort},fullNames{2,iCort},PrimReg(iCort,2),regNames{PrimReg(iCort,2)});
end

bins = 1:22;
figure(1);clf;nr = 4;nc =1;
subplot(nr,nc,1);
hist(PrimReg(:,1),bins);grid;set(gca,'Xlim',[0 23]);title('CorrNN: all ROIS');
subplot(nr,nc,2);
hist(PrimReg(1:60,1),bins);grid;set(gca,'Xlim',[0 23]);title('CorrNN: top 60 ROIS');
subplot(nr,nc,3);
hist(PrimReg(:,2),bins);grid;set(gca,'Xlim',[0 23]);title('normNN: all ROIS');
subplot(nr,nc,4);
hist(PrimReg(1:60,2),bins);grid;set(gca,'Xlim',[0 23]);title('normNN: top 60 ROIS');


figure(2);clf; % plot on the same axis for CorrNN and NomrNN 
bins = (1:23)-0.5;
histogram(PrimReg(1:60,1),bins);grid;set(gca,'Xlim',[0 23]);
hold on;
histogram(PrimReg(1:60,2),bins-0.1);grid;set(gca,'Xlim',[0 23]);grid; hold off;
set(gca,'XTick',[1:22]);set(gca,'FontSize',16);
legend('CorrNN','NormNN','Location','NorthWest');
