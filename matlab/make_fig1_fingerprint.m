% Make_Fig1_fingerprint
%
% Make up plots for Figure 1 of:
% Functional Connectome Fingerprinting Using Shallow Feedforward Neural Networks
% G. Sarar, B. Rao, T.T. Liu
%
% https://www.biorxiv.org/content/10.1101/2020.10.19.346189v1



%% make up filenames, using order that is consistent with prior code versions 
clear;

roiNum = [15 20 30 40 50 60]';
spanNum = [5:5:20 30 50:25:100 200:200:1000]';

nRoi = length(roiNum);
nSpans = length(spanNum);
suf1vec = {'corrNN','normNN'};
%suf1vec = {'corrNN'};
nTypes = length(suf1vec);
sdirvec = {'StatsData3/','StatsData3n/'};
for iType = 1:nTypes
    suf1 = suf1vec{iType};
    sdir = sdirvec{iType};
    for iRoi = 1:nRoi
        thisRoi = roiNum(iRoi);
        for iSpan = 1:nSpans
            thisSpan = spanNum(iSpan);
            
            if iType == 1
                fName = sprintf('%sfirst100Sub_statsSpan%dlen_%dROI_%s_tl_gsr1.pckl',sdir,thisSpan,thisRoi,suf1);
            else
                fName = sprintf('%sfirst100Sub_statsSpan%dlen_%dROI_%s_tl1_u256_sl0_g1_r0_gsr1.pckl',sdir,thisSpan,thisRoi,suf1);
            end
            
            fid = py.open(fName,'rb');
            data = py.pickle.load(fid);
            nPts = length(data);tmp = NaN*ones(nPts,2);
            for k = 1:nPts  % brute force reading of Python list -- there is probably a better way to do this
                tmp(k,:) = cell2mat(cell(data{k}));
            end
            tmp = 100*tmp;
            
            allStats{iType}(iSpan,iRoi) = mean(tmp(:,2));
            
        end
    end
end




%% do the plots here
if ~exist('doprint');doprint = 0; end
titlstr = {'CorrNN Mean Accuracy','NormNN Mean Accuracy'};
saveNames = {'FIG1_MeanCorrAll.png','FIG1_MeanNormAll.png'};
iGroup = 1;iParam = 1;lw = 3;fsize = 20;
roispan = 1:6;
iZoomout = 1;
for iType = 1:nTypes
    figure(200+iType);clf;
    thisData = allStats{iType}(:,roispan);
    semilogx(spanNum,thisData,'-o','LineWidth',lw);grid;
    set(gca,'Ylim',[0 105]);set(gca,'Xlim',[0 1050]);
    xlabel('Number of time points (N)');
    ylabel('Percent Accuracy');set(gca,'FontSize',fsize);
    legend(cellstr(num2str(roiNum(roispan))),'Location','SouthEast');
    set(gca,'YTick',[0:20:80 90 100]);
    set(gca,'XTick',[10 20 50 100 200 500 1000]);
    title(titlstr{iType});
    if doprint
    print(saveNames{iType},'-dpng');
    end
end