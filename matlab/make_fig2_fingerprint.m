% Script to generate Figures 2 plots of 
%
%
% Functional Connectome Fingerprinting Using Shallow Feedforward Neural Networks
% G. Sarar, B. Rao, T.T. Liu
%
% https://www.biorxiv.org/content/10.1101/2020.10.19.346189v1


%% make up filenames, using order that is consistent with prior code versions 
%clear;
roivals = [ 30   50 60  100 120 200 200 300 300 379 379 ];
spanvals = [200 200 100 100 50   50 30  20   34  27  16 ];
unitnum =  [326 256  1024 256 1024 512 6568 4096 8192 8192 14860];

nVals = length(roivals);
suf1vec = {'corrNN','normNN'};
%suf1vec = {'corrNN'};
nsuf1 = length(suf1vec);
for iSuf1 = 1:nsuf1
    suf1 = suf1vec{iSuf1};offset =(iSuf1-1)*nVals;
    for iVal = 1:nVals
        roiNum = roivals(iVal);
        spanNum = spanvals(iVal);
        uNum = unitnum(iVal);
      
        if iSuf1 == 1
            fnames{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl_gsr1.pckl',spanNum,roiNum,suf1);
            fnamesGS{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl_gsr0.pckl',spanNum,roiNum,suf1);
        else
            fnames{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl1_u%d_sl0_g1_r0_gsr1.pckl',spanNum,roiNum,suf1,uNum);
            fnamesGS{iVal+offset} = sprintf('first100Sub_statsSpan%dlen_%dROI_%s_tl1_u%d_sl0_g1_r0_gsr0.pckl',spanNum,roiNum,suf1,uNum);
        end
        
    end;
end

%% load in the data
sdir = 'StatsData2/';  % local directory where I've put a copy of the data from AWS
nFiles = length(fnames);
clear slData;

perf = NaN*ones(nFiles,1);
for iFile = 1:nFiles
    fid = py.open([sdir,fnames{iFile}],'rb');
    data = py.pickle.load(fid);
    nPts = length(data);tmp = NaN*ones(nPts,2);
    for k = 1:nPts  % brute force reading of Python list -- there is probably a better way to do this
        tmp(k,:) = 100*cell2mat(cell(data{k}));
    end
     allData{iFile} = tmp; 
     perf(iFile) = mean(tmp(:,2));
end

if nsuf1 > 1
    % CorrNN and NormNN
    perfAdd = [[roivals(:);roivals(:)] [spanvals(:);spanvals(:)] perf [NaN*ones(nVals,1); unitnum(:)]];
else
    % just do CorrNN
    perfAdd = [roivals(:) spanvals(:) perf NaN*ones(nVals,1)];
end


%% load in no GS data
nFilesGS = length(fnamesGS);
for iFile = 1:nFilesGS
    fid = py.open([sdir,fnamesGS{iFile}],'rb');
    data = py.pickle.load(fid);
    nPts = length(data);tmp = NaN*ones(nPts,2);
    for k = 1:nPts  % brute force reading of Python list -- there is probably a better way to do this
        tmp(k,:) = 100*cell2mat(cell(data{k}));
    end
     
     perfGS(iFile) = mean(tmp(:,2));
end


%% plot GS vs noGS for corrMAT;
perfGS = perfGS(:);
figure(1);clf;
subplot(2,1,1);
plot(perfGS(1:11),perf(1:11),'bo',[93 100],[93 100],'k-');grid;
axis([93 100 93 100]);
[H,P,CI,STATS] = ttest(perf(1:11)-perfGS(1:11))
delta1 = perf(1:11)-perfGS(1:11)
[mean(delta1) min(delta1) max(delta1) std(delta1)]
[roivals' spanvals' perf(1:11) perfGS(1:11)]

subplot(2,1,2);
plot(double(perfGS(12:22)),double(perf(12:22)),'bo',[93 100],[93 100],'k-');grid;
axis([93 100 93 100]);
[H,P,CI,STATS] = ttest(perf(12:22)-perfGS(12:22))
delta2 = (perf(12:22)-perfGS(12:22))
[mean(delta2) min(delta2) max(delta2) std(delta2)]
[roivals' spanvals' perf(12:22) perfGS(12:22)]

%%


addMode = 2;  % 0 = original plot; 1 = superimpose additional plots for normNN; 2 = replace plots for normNN
if ~exist('doprint');doprint = 0;end;
if ~exist('poption','var');poption = 'max380';end
if ~exist('dohist');dohist = 1;end;
if ~exist('addParamNum');addParamNum = 1;end;
iTypeVec = [ 1 2];   %  1 for CorrNN;  2 for NormNN
%iTypeVec = [ 1];
lval = [1 2 1 2 1 2 1 1 2 3 3  ];
nvals = length(roivals);

% Set parameters for different patch elements. 
sqsize = 10;
xsq = ([0 1 1 0]-0.5)*sqsize;
ysq = ([0 0 1 1]-0.5)*sqsize;
xd = [-sqrt(2)/2 0 sqrt(2)/2 0]*sqsize;
yd = [0 sqrt(2)/2 0 -sqrt(2)/2]*sqsize;
xt = [-0.5 0.5 0]*sqsize;
yt = [-0.5  -0.5 0.5]*sqsize;


iStart1 = 1;
iStart2 = nVals+1;

% set up parameters to indicate how graph looks
tvals = 11;
x = [1:400];
x2 = [1:400];
span1 = [iStart1:(iStart1+tvals-1)];span2 = [iStart2:(iStart2+tvals-1)];

xax = [0 400];
cax = [95 100];
lval = [1 2 1 2 1 2 1 1 2 2 1];
nPos = [1 1 1 2 2 2 1 2 2 2 2]; % 1 = below; 2 = in-line
supp = [4 0 5 0 6 3 7 8 2 1 9];  % add supplementary index, with index into iSupp indicated.
         

showhist = ones(20,1); % just default to show histograms. 

% more plotting parameters
xinc = [-27 12 -30 -15 8  10 8 -28 -20 -28 -28];
yinc = [10 -5 12 -12 8 10  -10 -8   20  20  -4 ];
showhist = [0   1 1   1  1  1  1 0  1  1 0];
hxinc = [-20 -5 -50 -25 -70 -90 -100 -120 -130 -160 8];
hyinc = [-25  15 -25  12 -25 25 -22 -15 50-34+20 30+12 8];
suppInc = [-28 -32];
suppIncy = [0  5];
suppIncyh = [0  5];
   
% define solid curves to show contours of 6,000 and 10,000 points
y = 10000./x;
y2 = 6000./x;lw = 3;
xwid = 0.08;
ywid = 0.05;


snames = {'Fig2A','Fig2B'};
titlestr = {'A. CorrNN Performance','B. NormNN Performance'};
doPlotGS = 0;
    
    for iType = iTypeVec  % corrNN and then  normNN
        figure(300+iType);clf;
        if iType == 1
            useperf = perfAdd(span1,:);usespan = span1;
        else
            useperf =perfAdd(span2,:);usespan = span2;
        end
        
        plot(x,y,'k-',x2,y2,'b-','LineWidth',lw);set(gca,'Ylim',[0 250],'Xlim',xax);
        hold on;
        
        colormap(jet(256)); caxis(cax);
        
        % loop through the data points 
        for k = 1:size(useperf,1);    
            u1 = useperf(k,1);u2 = useperf(k,2);u3 = useperf(k,3);
            % determine patch type
            switch lval(k)
                case 1
                    xb = xsq;yb = ysq;
                case 2
                    xb =xd; yb = yd;
                otherwise
                    xb = xt; yb = yt;
            end
            
            % plot the patch and text labels
            patch(xb+u1, yb+u2,u3);
            ht = text(useperf(k,1)+xinc(k),useperf(k,2)+yinc(k),sprintf('%.2f',useperf(k,3)));
            set(ht,'Fontsize',20);
            
            if addParamNum
                % number of ROIs and units
                thisM = useperf(k,1);
                thisK = useperf(k,4);
                %number of subjects
                L = 100;
                            
                % calculate number of model params
                [aN,bN] = modnum(thisK,thisM,L);
                if iType == 1  %corrNN params
                    thispNum = aN;
                else  %normNN params
                    thispNum = bN;
                end
                
                % report in units of thousands
                thispNum =round(thispNum/1e3);
                if iType == 1
                    hp = text(useperf(k,1)+xinc(k),useperf(k,2)+yinc(k)-8,sprintf('%dk',thispNum));
                else
                    if nPos(k) == 1   % below
                        hp = text(useperf(k,1)+xinc(k),useperf(k,2)+yinc(k)-8,sprintf('%dk',thispNum));
                        hp2 = text(useperf(k,1)+xinc(k),useperf(k,2)+yinc(k)-16,sprintf('K=%d',round(thisK)));
                        set(hp2,'Fontsize',16);
                    else  %in-line
                        hp = text(useperf(k,1)+xinc(k),useperf(k,2)+yinc(k)-8,sprintf('%dk; K=%d',thispNum,round(thisK)));
                    end 
                end
                set(hp,'Fontsize',16);
            end
        end
        
        xlabel('Number of ROIs');ylabel('Span Length');
        %title(titlstr{sInd});
        set(gca,'FontSize',20)
        grid;
        hc = colorbar;
        legend('10,000','6,000','Location','NorthEast');
        title(titlestr{iType},'FontSize',32);
        
        if dohist
            pos =plotboxpos(gca);
            % plot histograms
            for k = 1:size(useperf);
                if showhist(k)
                    thisInd  = k+(iType-1)*nVals;
                    xnorm = (roivals(k)+hxinc(k))/250*pos(3) + pos(1);
                    ynorm = (spanvals(k)+hyinc(k))/250*pos(4)+pos(2);
                    ax = axes('Position',[xnorm,ynorm,xwid,ywid]);
                    hn = histogram(allData{thisInd}(:,2));grid;
                    if hn.NumBins == 1
                        hn = histogram(allData{thisInd}(:,2),[97.5000 98.5000 99.5000 100.5000]);grid;
                    end
                    
                    set(gca,'FontSize',16);         
                end
            end
            
        end
        
        if doPlotGS  % plot GS vs nonGS
            hg(iType) = axes('Position',[0.55 0.55 0.25 0.25]);
            
            hpgs = plot(perfGS(usespan),perf(usespan),'bo');
            set(hpgs,'MarkerFaceColor','b');
            hold on;plot([93 100],[93 100],'k-');grid;hold off;
            set(gca,'FontSize',14);
            xlabel('Without GSR');ylabel('With GSR');
            axis([93 100 93 100]);
        end
        
        
        if doprint
            print([snames{iType},poption,'_g',num2str(doPlotGS),'.png'],'-dpng');
            print([snames{iType},poption,'_g',num2str(doPlotGS),'.eps'],'-depsc');
            print([snames{iType},poption,'_g',num2str(doPlotGS),'.pdf'],'-dpdf');
        end
        
    end
    
    

