function [cN,nN,rN,Keq,Neq,nN2] = modnum(K,M,L)

% computes number of model parameters for Figure 2 of
% Functional Connectome Fingerprinting Using Shallow Feedforward Neural Networks
% G. Sarar, B. Rao, T.T. Liu
%
% https://www.biorxiv.org/content/10.1101/2020.10.19.346189v1


% # parameters for corrNN
cN = 0.5*L.*M.^2 -0.5*L.*M + 3*L;
% # parameters for normNN
nN = K.*M + K.*(L+3) + 3*L;
% # parameters for normNN with layer of random weights. 
nN2 = K.*(L+2) + 3*L;

% ratio of corrNN num params and normNN num params
rN = cN./nN;

% number of units for which nomrNN has same number of parameters as corrNN
Keq = (0.5*L.*M.^2 -0.5*L.*M)./(M+L+3);
% as a sanity check:  number of nomrNN paramers
Neq = Keq.*M + Keq.*(L+3) + 3*L;

% recompute values for when we don't need bias terms in dense layers. 


end