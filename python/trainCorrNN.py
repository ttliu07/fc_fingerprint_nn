# Python file to demonstrate training of the CorrNN model in
# Functional Connecotome Fingeprinting with Shallow Feedfoward Neural Networks
# https://doi.org/10.1101/2020.10.19.346189
#
# Developers: G. Sarar and T.T. Liu, Center for Functional MRI, University of California San Diego
#
# This software is Copyright (c) 2021 The Regents of the University of California. All Rights Reserved. Permission to copy,
# modify, and distribute this software and its documentation for educational, research and non-profit purposes, without fee,# and without a written agreement is hereby granted, provided that the above copyright notice, this paragraph and the
# following three paragraphs appear in all copies. Permission to make commercial use of this software may be obtained
# by contacting:
#
# Office of Innovation and Commercialization
# 9500 Gilman Drive, Mail Code 0910
# University of California
# La Jolla, CA 92093-0910
# (858) 534-5815
# invent@ucsd.edu
#
# This software program and documentation are copyrighted by The Regents of the University of California.
# The software program and documentation are supplied "as is", without any accompanying services from The Regents.
# The Regents does not warrant that the operation of the program will be uninterrupted or error-free.
# The end-user understands that the program was developed for research purposes and is advised not to rely
# exclusively on the program for any reason.
#
# IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
# OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
# EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA
# SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
#
#-------------------
# Data requirements:
#-------------------
#  Preprocessed rsfMRI MATLAB *.mat files (downloadable from https://osf.io/uj2n7/ )
#    S500_alldata_with_subcort_normalized_GS_projected_out_new.mat
#    S500_alldata_with_subcort_normalized_GS_projected_out_second100.mat
#    S500_alldata_with_subcort_normalized.mat
#  
#  pckl files:  available in the matlab/ROIdata subdirectory of this repo:
#               https://bitbucket.org/ttliu07/fc_fingerprint_nn/src/master/
#    indexesPerm_correlation_greedy_ROI_l2_findingIndexesResults.pckl
#-------------
# USAGE NOTES:
#-------------
#
#  1.  tested using Tensorflow with Python 2.7; move the required data files to the current directory (or create symbolic links) 
#  2.  to choose between first 100  and second 100 group of subjects, you will need to comment in
#      and comment out the relevant lines
#      (search on the work "pick" for where to do this -- sorry about the clunky legacy code). 
#  3. specify parameters (see comments below).  The parameters shown here will do the combinations in Figure 2. 
#  4. for each combination there will be *.hdf5 with the model weights and a *.pckl file generated with summary data.
#  5. the *.hdf5 weights can be used for testing of the model. 


from keras.models import Sequential
from keras.layers import Lambda, Activation,Dense, Dropout, RNN, GRU, AveragePooling1D,BatchNormalization
from keras import optimizers
from keras import utils
from keras.callbacks import TensorBoard, ModelCheckpoint, Callback, LearningRateScheduler, EarlyStopping
import pdb
import h5py
from time import time
import math
import numpy as np
#from joblib import Parallel,delayed
import multiprocessing
import tensorflow as tf
import keras
#from keras.backend import tensorflow_backend as K
from keras import backend as K
import pickle

#K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=8, inter_op_parallelism_threads=8)))

class ValBatch(Callback):
    def __init__(self, validation_data):
        self.validation_data=validation_data
    def on_batch_end(self, batch, logs={}):
        if batch%100==0:
            x,y = self.validation_data
            loss, acc = self.model.evaluate(x, y, verbose=0)
            print('\nValidation loss: {}, acc: {}\n'.format(loss, acc))

def demean_and_normalize(three_d_matrix):
    for x in range(three_d_matrix.shape[0]):
        for y in range(three_d_matrix.shape[2]):
            sliver=three_d_matrix[x, :, y]
            avg=np.average(sliver)
            std=np.std(sliver)
            three_d_matrix[x, :, y] = three_d_matrix[x, :, y]-avg
            three_d_matrix[x, :, y] = three_d_matrix[x, :, y]/std

    return three_d_matrix

def demean_and_normalize_fast(three_d_matrix):
    three_d_matrix=three_d_matrix-np.mean(three_d_matrix,axis=1).reshape(three_d_matrix.shape[0],1,three_d_matrix.shape[2])
    three_d_matrix=three_d_matrix/np.std(three_d_matrix,axis=1).reshape(three_d_matrix.shape[0],1,three_d_matrix.shape[2])
    return three_d_matrix

def generate_arrays_from_file():
    
    if doGSR > 0:
    # pick first or second 100 here 
        f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new.mat', 'r')
       # f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_second100.mat', 'r')
    else:   #nonGSR currently only supported for first 100  
        f = h5py.File('S500_alldata_with_subcort_normalized.mat', 'r')
        print('generation: loading nonGSR data')
        
    data = f.get('alldata')
    #Convert to numpy array
    data = np.array(data)
    data = np.concatenate((data[:, 0, :, :], data[:, 1, :, :]), axis=0)

#

    indices=np.zeros((200*(1200-span),2), dtype=int)
    

    keep_track=0
    for x in range(200):
        for y in range(1200-span):
            indices[keep_track, :]=[x, y]
            keep_track+=1

    #print(indices)

    labels=np.zeros((200*(1200-span)), dtype=int)

    for x in range(200):
        labels[x*(1200-span):x*(1200-span)+(1200-span)] = x % 100

#

    p=np.random.permutation(indices.shape[0])
    print(p)
    labels=labels[p]
    indices=indices[p]
    index=0
    
    while True:
        lenInputInd=len(input_indexes)
        batch=np.zeros((b_size,(lenInputInd*lenInputInd-lenInputInd)/2))
        #print(batch.shape())
        batch_labels=np.zeros((b_size))
        for i in range(b_size):
            ryan_howard=indices[index, :]
            sliver=data[ryan_howard[0], ryan_howard[1]: ryan_howard[1]+span, :]
            sliver=sliver[:,input_indexes]
            corrMat=np.corrcoef(sliver,rowvar=0)
            batch[i, :] = corrMat[np.triu_indices(corrMat.shape[0],1)]
            
            batch_labels[i]=labels[index]
            index += 1
            if index >= 200*(1200-span):
                index=0
                p = np.random.permutation(indices.shape[0])

                labels = labels[p]
                indices = indices[p]
                print(p)
        yield batch, utils.to_categorical(batch_labels, num_classes=100)
        
def generate_validation():
    
    if doGSR > 0:
    #PICK 1st 100 or 2nd 100 here 
        f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new.mat', 'r')
      #  f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_second100.mat', 'r')
    else:
        f = h5py.File('S500_alldata_with_subcort_normalized.mat', 'r')
        print('validation: loading nonGSR')
    data = f.get('alldata')
    #Convert to numpy array
    data = np.array(data[:, 2, :, :])
#

    indices=np.zeros((100*(1200-span),2), dtype=int)
    

    keep_track=0
    for x in range(100):
        for y in range(1200-span):
            indices[keep_track, :]=[x, y]
            keep_track+=1

    #print(indices)

    labels=np.zeros((100*(1200-span)), dtype=int)

    for x in range(100):
        labels[x*(1200-span):x*(1200-span)+(1200-span)] = x % 100

#

    p=np.random.permutation(indices.shape[0])
    print('val',p)
    labels=labels[p]
    indices=indices[p]
    index=0
    
    while True:
        lenInputInd=len(input_indexes)
        batch=np.zeros((b_size,(lenInputInd*lenInputInd-lenInputInd)/2))
        #print(batch.shape())
        batch_labels=np.zeros((b_size))
        for i in range(b_size):
            ryan_howard=indices[index, :]
            sliver=data[ryan_howard[0], ryan_howard[1]: ryan_howard[1]+span, :]
            sliver=sliver[:,input_indexes]
            corrMat=np.corrcoef(sliver,rowvar=0)
            batch[i, :] = corrMat[np.triu_indices(corrMat.shape[0],1)]
            
            batch_labels[i]=labels[index]
            index += 1
            if index >= 100*(1200-span):
                index=0
                p = np.random.permutation(indices.shape[0])

                labels = labels[p]
                indices = indices[p]
                print('val',p)
        yield batch, utils.to_categorical(batch_labels, num_classes=100)

def step_decay(epoch):
    initial_lrate = 0.001
    drop = 0.5
    epochs_drop = 100.0
    lrate = initial_lrate * math.pow(drop,math.floor((1+epoch)/epochs_drop))
    return lrate

#Define hyperparameters and other details for the model
drop_out=0.25 #0.45
r_drop_out=0.25 #0.45    
learning_rate=0.001 #0.001
#learning_rate=LearningRateScheduler(step_decay)
b1=0.9
b2=0.999
b_size=64
num_epochs=4000
#IT WAS WRONG
steps_per_epoch=600
DATASET=2
lr_decay = LearningRateScheduler(schedule=lambda epoch: args.lr * (0.9 ** epoch))
lrate=LearningRateScheduler(step_decay)


tbCallBack=TensorBoard(log_dir="logs/"+"spe"+str(steps_per_epoch)+"ds"+str(DATASET)+"do"+str(drop_out)+"rdo"+str(r_drop_out)+
                       "lr"+str(learning_rate)+
                        "b1"+str(b1)+"b2"+str(b2)+
                        "bsize"+str(b_size)+
                        "numep"+str(num_epochs)
                       , write_graph=False)

file = open('indexesPerm_correlation_greedy_ROI_l2_findingIndexesResults.pckl','rb')
indexes_data = pickle.load(file)
good_indexes=np.array(indexes_data['killedIndexes'])
# # close the file
file.close()

earlystop=EarlyStopping(monitor='val_loss', min_delta=0, patience=30, verbose=0, mode='auto')

# SPECIFY PARAMETERS HERE -- specify either loopMode = 0 or 1 and reorder or comment out code as needed. 
doGSR = 1
#loopMode = 0 example specification for nested looping
# this example will do (number of spans)x(number of ROIs) = 13x6 = 78 combinations
# these are the combinations shown in Figure 1 of the paper. 
loopMode=0
span_list=[5,10,15,20,30,50,75,100,200,400,600,800,1000]
ROInum_list=[15,20,30,40,50,60]

#loopMode = 1 example specification for tracked looping.
#this example will do 11 combinations from Figure 2 in the paper 
loopMode=1
roi_ind =0
span_list= [ 200,200,100,100,50, 50, 30, 34, 20, 27, 16]
ROInum_list2=[30,50, 60, 100,120,200,200,300,300,379,379]
ROInum_list = [0] #dummy value


for span in span_list:
    for ROInum in ROInum_list:
        if loopMode==1:
            ROInum=ROInum_list2[roi_ind]
            roi_ind = roi_ind+1
        #NOTE: this "if"  structure assumes that we did loopMode =1 first,
        #      so we don't need to redo certain combinations for loopmode = 0 (or vice versa)
        if (loopMode == 0) and ((ROInum == 30 and span == 200) or (ROInum == 50 and span== 200) or (ROInum == 60 and span == 100)):
            continue
        else:
            input_indexes=good_indexes[-ROInum:]

            lenInputInd=len(input_indexes)
# pick first or second 100 here 
            checkCallBack=ModelCheckpoint('corrMat_singleLayer_orig_span%d_%dROI_fromCorrGreedyPerm_gsr%d.best.hdf5'%(span,ROInum,doGSR), monitor='val_acc', save_best_only=True)
#            checkCallBack=ModelCheckpoint('second100_corrMat_singleLayer_orig_span%d_%dROI_fromCorrGreedyPerm.best.hdf5'%(span,ROInum), monitor='val_acc', save_best_only=True)

            model=Sequential()
            model.add(Dense(100,input_shape=((lenInputInd*lenInputInd-lenInputInd)/2,)))
            model.add(BatchNormalization())
            model.add(Activation('softmax'))


            adam_optimizer=optimizers.Adam(lr=learning_rate, beta_1=b1, beta_2=b2)

            model.compile(optimizer=adam_optimizer,
                     loss='categorical_crossentropy',
                     metrics=['accuracy'])
            print(model.summary())
    #         pdb.set_trace()
            history=model.fit_generator(generate_arrays_from_file(), steps_per_epoch=steps_per_epoch,
                  validation_data=generate_validation(), epochs=num_epochs,validation_steps=70,callbacks=[tbCallBack, checkCallBack, lrate, earlystop])
            #, earlystop
            #PICK first 100 or 2nd 100 here 
#            pickle.dump(history.history,open("second100_corrMat_singleLayer_orig_span%d_%dROI_fromCorrGreedyPerm"%(span,ROInum),"wb"))
            pickle.dump(history.history,open("corrMat_singleLayer_orig_span%d_%dROI_fromCorrGreedyPerm_gsr%d"%(span,ROInum,doGSR),"wb"))        


