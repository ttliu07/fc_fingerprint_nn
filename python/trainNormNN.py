# Python file to demonstrate training of the NormNN model in
# Functional Connecotome Fingeprinting with Shallow Feedfoward Neural Networks
# https://doi.org/10.1101/2020.10.19.346189
#
# Developers: G. Sarar and T.T. Liu, Center for Functional MRI, University of California San Diego
#
# This software is Copyright (c)  2021 The Regents of the University of California. All Rights Reserved. Permission to copy,
# modify, and distribute this software and its documentation for educational, research and non-profit purposes, without fee,# and without a written agreement is hereby granted, provided that the above copyright notice, this paragraph and the
# following three paragraphs appear in all copies. Permission to make commercial use of this software may be obtained
# by contacting:
#
# Office of Innovation and Commercialization
# 9500 Gilman Drive, Mail Code 0910
# University of California
# La Jolla, CA 92093-0910
# (858) 534-5815
# invent@ucsd.edu
#
# This software program and documentation are copyrighted by The Regents of the University of California.
# The software program and documentation are supplied "as is", without any accompanying services from The Regents.
# The Regents does not warrant that the operation of the program will be uninterrupted or error-free.
# The end-user understands that the program was developed for research purposes and is advised not to rely
# exclusively on the program for any reason.
#
# IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
# OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
# EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA
# SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
#
#-------------------
# Data requirements:
#-------------------
#  Preprocessed rsfMRI MATLAB *.mat files (downloadable from https://osf.io/uj2n7/ )
#    S500_alldata_with_subcort_normalized_GS_projected_out_new.mat
#    S500_alldata_with_subcort_normalized_GS_projected_out_second100.mat
#    S500_alldata_with_subcort_normalized.mat
#  
#  pckl files:  available in the matlab/ROIdata subdirectory of this repo:
#               https://bitbucket.org/ttliu07/fc_fingerprint_nn/src/master/
#    greedy_ROI_l2_findingIndexesResults.pckl
#-------------
# USAGE NOTES:
#-------------
#
#  1. tested using Tensorflow with Python 2.7; move the required data files to the current directory (or create symbolic links) 
#  2. set up parameters -- see comments below. The parameters shown here will do the combos shown in Figure 2 of the paper. 
#  4. for each combination there will be *.hdf5 with the model weights and a *.pckl file generated with summary data.
#  5. the *.hdf5 weights can be used for testing of the model. 


from keras.models import Sequential
from keras.layers import Lambda, Activation,Dense, Dropout, RNN, GRU, AveragePooling1D,BatchNormalization
from keras import optimizers
from keras import utils
from keras.callbacks import TensorBoard, ModelCheckpoint, Callback, LearningRateScheduler, EarlyStopping
import pdb
import h5py
from time import time
import math
import numpy as np
#from joblib import Parallel,delayed
import multiprocessing
import tensorflow as tf
import keras
#from keras.backend import tensorflow_backend as K
from keras import backend as K
import pickle

#K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=8, inter_op_parallelism_threads=8)))

class ValBatch(Callback):
    def __init__(self, validation_data):
        self.validation_data=validation_data
    def on_batch_end(self, batch, logs={}):
        if batch%100==0:
            x,y = self.validation_data
            loss, acc = self.model.evaluate(x, y, verbose=0)
            print('\nValidation loss: {}, acc: {}\n'.format(loss, acc))

def demean_and_normalize(three_d_matrix):
    for x in range(three_d_matrix.shape[0]):
        for y in range(three_d_matrix.shape[2]):
            sliver=three_d_matrix[x, :, y]
            avg=np.average(sliver)
            std=np.std(sliver)
            three_d_matrix[x, :, y] = three_d_matrix[x, :, y]-avg
            three_d_matrix[x, :, y] = three_d_matrix[x, :, y]/std

    return three_d_matrix

def demean_and_normalize_fast(three_d_matrix):
    three_d_matrix=three_d_matrix-np.mean(three_d_matrix,axis=1).reshape(three_d_matrix.shape[0],1,three_d_matrix.shape[2])
    three_d_matrix=three_d_matrix/np.std(three_d_matrix,axis=1).reshape(three_d_matrix.shape[0],1,three_d_matrix.shape[2])
    return three_d_matrix

def generate_arrays_from_file(group):
    if doGS == 1:
        if group == 1:
            print('generating array from first 100 with GSR')
            f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new.mat', 'r')
        else:
            print('generating array from second 100 with GSR')
            f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new_second100.mat', 'r')
    else:
        print('generating array from first 100 without  GSR')
        f = h5py.File('S500_alldata_with_subcort_normalized.mat', 'r')
        
    data = f.get('alldata')
    #Convert to numpy array
    data = np.array(data)
    data = np.concatenate((data[:, 0, :, :], data[:, 1, :, :]), axis=0)

#

    indices=np.zeros((200*(1200-span),2), dtype=int)
    

    keep_track=0
    for x in range(200):
        for y in range(1200-span):
            indices[keep_track, :]=[x, y]
            keep_track+=1

    #print(indices)

    labels=np.zeros((200*(1200-span)), dtype=int)

    for x in range(200):
        labels[x*(1200-span):x*(1200-span)+(1200-span)] = x % 100

#

    p=np.random.permutation(indices.shape[0])
    print(p)
    labels=labels[p]
    indices=indices[p]
    index=0
    
    while True:
        batch=np.zeros((b_size,span,len(input_indexes)))
        #print(batch.shape())
        batch_labels=np.zeros((b_size))
        for i in range(b_size):
            ryan_howard=indices[index, :]

            sliver=data[ryan_howard[0], ryan_howard[1]: ryan_howard[1]+span, :]
            sliver=sliver[:,input_indexes]
            batch[i, :, :] = sliver #/ std
            batch_labels[i]=labels[index]
            index += 1
            if index >= 200*(1200-span):
                index=0
                p = np.random.permutation(indices.shape[0])

                labels = labels[p]
                indices = indices[p]
                print(p)
        batch=demean_and_normalize_fast(batch)
        yield batch, utils.to_categorical(batch_labels, num_classes=100)
        
def generate_validation(group):
    if doGS == 1:
        if group == 1:
            print('generating validation from first 100 with GS')
            f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new.mat', 'r')
        else:
            print('generating validation from second 100 with GS');
            f = h5py.File('S500_alldata_with_subcort_normalized_GS_projected_out_new_second100.mat', 'r')
    else:
            print('generating validation from first 100 without  GS')
            f = h5py.File('S500_alldata_with_subcort_normalized.mat', 'r')
            
    data = f.get('alldata')
    #Convert to numpy array
    data = np.array(data[:, 2, :, :])
#

    indices=np.zeros((100*(1200-span),2), dtype=int)
    

    keep_track=0
    for x in range(100):
        for y in range(1200-span):
            indices[keep_track, :]=[x, y]
            keep_track+=1

    #print(indices)

    labels=np.zeros((100*(1200-span)), dtype=int)

    for x in range(100):
        labels[x*(1200-span):x*(1200-span)+(1200-span)] = x % 100

#

    p=np.random.permutation(indices.shape[0])
    print('val',p)
    labels=labels[p]
    indices=indices[p]
    index=0
    
    #
    while True:
        batch=np.zeros((b_size,span,len(input_indexes)))
        #print(batch.shape())
        batch_labels=np.zeros((b_size))
        for i in range(b_size):
            ryan_howard=indices[index, :]

            sliver=data[ryan_howard[0], ryan_howard[1]: ryan_howard[1]+span, :]
            sliver=sliver[:,input_indexes]
            batch[i, :, :] = sliver #/ std
            batch_labels[i]=labels[index]
            index += 1
            if index >= 100*(1200-span):
                index=0
                p = np.random.permutation(indices.shape[0])

                labels = labels[p]
                indices = indices[p]
                print('val',p)
        batch=demean_and_normalize_fast(batch)
        yield batch, utils.to_categorical(batch_labels, num_classes=100)
    #

def step_decay(epoch):
    initial_lrate = 0.001
    drop = 0.5
    epochs_drop = 100.0
    lrate = initial_lrate * math.pow(drop,math.floor((1+epoch)/epochs_drop))
    return lrate

#Define hyperparameters and other details for the model
drop_out=0.25 #0.45
r_drop_out=0.25 #0.45    
learning_rate=0.001 #0.001
#learning_rate=LearningRateScheduler(step_decay)
b1=0.9
b2=0.999
b_size=64
num_epochs=4000
#IT WAS WRONG
steps_per_epoch=600
DATASET=2
lr_decay = LearningRateScheduler(schedule=lambda epoch: args.lr * (0.9 ** epoch))
lrate=LearningRateScheduler(step_decay)


tbCallBack=TensorBoard(log_dir="logs/"+"spe"+str(steps_per_epoch)+"ds"+str(DATASET)+"do"+str(drop_out)+"rdo"+str(r_drop_out)+
                       "lr"+str(learning_rate)+
                        "b1"+str(b1)+"b2"+str(b2)+
                        "bsize"+str(b_size)+
                        "numep"+str(num_epochs)
                       , write_graph=False)


file = open('greedy_ROI_l2_findingIndexesResults.pckl','rb')
indexes_data = pickle.load(file)
good_indexes=np.array(indexes_data['killedIndexes'])
# # close the file
file.close()

earlystop=EarlyStopping(monitor='val_loss', min_delta=0, patience=30, verbose=0, mode='auto')


#SET UP PARAMETERS HERE!!!!!




# specify whether we are doing the first 100 or second 100 
# group 1: first 100;  2: second 100
group_list = [1]

#  define entries in save_list to determine whether we will use a previously computed saved layer. 
# 0: default model;
# 1: saved layer with bias
# 2: saved layer without bias
# 3: randomize ROI order of saved layer
# 4: use random Gaussian first layer; no bias term
# 5: use use random weights from 4, but normalize so all units have norm 1
# 6: apply the random weights from 4 from first 100 to second 100. (use with group = 2)
# 7: apply and normalized random weights from 4 from first 100 to second 100. (use with group = 2)
save_list = [0]

# specify how many random combinations we want to do. 
#randLoop = list(range(5))
randLoop = [0]
print(randLoop)

# scale to use for scaling amplitude of randomized layers. 
wscale = 0.15   # scale for 50 ROIs
wscale = 0.07    # scale for 379 ROIs, 200 ROIS

# determine whether GSR is used
doGS = 1

# loopmode parameter -- legacy functionality
loopmode = 0   # set to 1 if we want to step through save_list_vec in sync with group_list -- Not really used, so leave it at 0. 
this_ind = 0
save_list_vec = [0,1,2] 

# loopmode2 is used to control how we move through combinations of span length and ROI number. 
loopmode2 = 1 # 0: uses span_list and ROInum_list and uni_list in nested fashion -->  Figure 1. 
              # 1: make ROInum and unitNum track entries in span_list -->  Figure 2
              

# example params for loopmode2 = 0
# this example will do (number of spans)x(number of ROIs) = 13x6 = 78 combinations
# these are the combinations shown in Figure 1 of the paper.
span_list=[5,10,15,20,30,50,75,100,200,400,600,800,1000]
ROInum_list=[15,20,30,40,50,60]
uni_list= [256];

# example params for loopmode2 = 1
# 
span_list= [ 100,100,200,200,50,50,30,34,20,27,16]
ROInum_list2=[100,60,30, 50, 120, 200,200,300,300,379,379]
unitnum_list2 = [256,512,326,256,1024,512,4096,8192,4096,8192,14860]


for group in group_list:
    if group == 1:
        gprefix = 'first100'
    else:
        gprefix = 'second100'

    if loopmode2 ==  1:   # set dummy values for this mode
        ROInum_list = [0]
        uni_list = [0]
        roi_ind = 0
        
    for useSavedFirstLayer in save_list:
        for span in span_list:
            for ROInum in ROInum_list:
                 #NOTE: this "if"  structure assumes that we did loopMode =1 first,
                 #      so we don't need to redo certain combinations for loopmode = 0 (or vice versa)
                if (loopmode2 == 0) and ((ROInum == 30 and span == 200) or (ROInum == 50 and span== 200) or (ROInum == 60 and span == 100)):
                    continue
                else:
                    for unitNum in uni_list:

                        if loopmode2 == 1:
                            ROInum = ROInum_list2[roi_ind]
                            unitNum = unitnum_list2[roi_ind]
                            roi_ind = roi_ind+1

                        for iRand in randLoop:
                            if loopmode == 1:
                                useSavedFirstLayer = save_list_vec[this_ind]
                                this_ind = this_ind+1
                            if useSavedFirstLayer >= 1 and useSavedFirstLayer < 4:
                                print("Loading Model Weights from first 100 to get Saved First Layer")
                                fname=h5py.File('saved_weights2/first100_unit%d_span%d_%dROI_sl0_r0_gsr%d_.best.hdf5'%(unitNum,span,ROInum,doGS),'r')
                            
                                dl1 = fname['/model_weights/dense_1/dense_1']
                                bias1= dl1.get('bias:0').value
                                kernel1= dl1.get('kernel:0').value
                                fname.close()
                            
                                if useSavedFirstLayer == 3:
                                    print("Randomly permuting the kernel")
                                    iRand=np.random.permutation(kernel1.shape[0])
                                    print(iRand)
                                    print(kernel1[0,0:5])
                                    kernel1=kernel1[iRand,:]
                                    print(kernel1[0,0:5])
                            
                            elif useSavedFirstLayer == 4:
                                print("Using random first layer kernel")
                                kernel1=np.float32(wscale*np.random.randn(ROInum,unitNum))
                                pickle.dump(kernel1,open("normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,useSavedFirstLayer,group,np.int8(100*wscale),iRand,doGS),"wb"))
                            
                            elif useSavedFirstLayer == 5:
                                print("Using and normalizing previously saved random first layer kernel")
                                kernel1 = pickle.load(open("saved_weights2/normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,useSavedFirstLayer-1,group,np.int8(100*wscale),iRand,doGS),"rb"))
                                nn = np.linalg.norm(kernel1,axis=0)
                                kernel1 = kernel1/nn
                                pickle.dump(kernel1,open("normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,useSavedFirstLayer,group,np.int8(100*wscale),iRand,doGS),"wb"))
            
                            elif useSavedFirstLayer == 6:
                                print("Using previously saved random first layer kernel from first 100")
                                kernel1 = pickle.load(open("saved_weights2/normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,4,1,np.int8(100*wscale),iRand,doGS),"rb"))
                            elif useSavedFirstLayer == 7:
                                print("Using  and normalzing previously saved random first layer kernel from first 100")
                                kernel1 = pickle.load(open("saved_weights2/normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,4,1,np.int8(100*wscale),iRand,doGS),"rb"))
                                nn = np.linalg.norm(kernel1,axis=0)
                                kernel1 = kernel1/nn
                                pickle.dump(kernel1,open("normNN_u%d_%dROI%dtpts_rand_sl%dg%d_sc%d_r%d_gsr%d.p"%(unitNum,ROInum,span,useSavedFirstLayer,group,np.int8(100*wscale),iRand,doGS),"wb"))

                            input_indexes=good_indexes[-ROInum:]
                            lenInputInd=len(input_indexes)

                            checkCallBack=ModelCheckpoint('%s_normNN_unit%d_span%d_%dROI_sl%d_r%d_gsr%d.best.hdf5'%(gprefix,unitNum,span,ROInum,useSavedFirstLayer,iRand,doGS), monitor='val_acc', save_best_only=True)
          

                            model=Sequential()
                            if useSavedFirstLayer == 1:
                                print("Using Saved First Layer")
                                model.add(Lambda(lambda x:K.dot(x,tf.constant(kernel1)),input_shape=(span,ROInum)))
                                model.add(Lambda(lambda x:K.bias_add(x,tf.constant(bias1)),input_shape=(unitNum,)))
                            elif useSavedFirstLayer == 2 or useSavedFirstLayer >= 3:
                                print("Using Saved or Random First Layer without Bias")
                                model.add(Lambda(lambda x:K.dot(x,tf.constant(kernel1)),input_shape=(span,ROInum)))
                            else:
                                print("Adding default dense layer")
                                model.add(Dense(unitNum,input_shape=(span,ROInum),name="dense_1"))
                    
                            model.add(Lambda(lambda x:x**2))
                            model.add(Lambda(lambda x:K.sum(x,axis=1)))
                            model.add(Lambda(lambda x:K.sqrt(x)))
      
                            model.add(BatchNormalization(name="batch_normalization_1"))
                            model.add(Dense(100,name="dense_2"))
                            model.add(BatchNormalization(name="batch_normalization_2"))             
                            model.add(Activation('softmax'))

                            adam_optimizer=optimizers.Adam(lr=learning_rate, beta_1=b1, beta_2=b2)

                            model.compile(optimizer=adam_optimizer,
                             loss='categorical_crossentropy',
                             metrics=['accuracy'])
                            print(model.summary())

                            history=model.fit_generator(generate_arrays_from_file(group), steps_per_epoch=steps_per_epoch,
                  validation_data=generate_validation(group), epochs=num_epochs,validation_steps=70,callbacks=[tbCallBack, checkCallBack, lrate, earlystop])

                            pickle.dump(history.history,open("%s_normNN_unit%d_span%d_%dROI_sl%d_r%d_gsr%d"%(gprefix,unitNum,span,ROInum,useSavedFirstLayer,iRand,doGS),"wb")) 

                            K.clear_session()

